
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author edgar
 */
public class Eventos {

    static Departamentos Departamento = ProyectoBD.getDepartamentos();
    static ProyectoBD Productos = InicioSesion.getProductos();
    static Ventas TablaVentas = ProyectoBD.getVentas();
    static Compras Compras = ProyectoBD.getCompras();

    public static void llenarTabla(String t) {

        DefaultTableModel modeloTabla = null;

        if (Productos.Tabla.equalsIgnoreCase("Productos")) {
            modeloTabla = (DefaultTableModel) Productos.TablaProductos.getModel();
            System.out.println(Productos.Tabla);

        } else if (Productos.Tabla.equalsIgnoreCase("Venta")) {
            modeloTabla = (DefaultTableModel) TablaVentas.TablaVentas.getModel();

        } else if (Productos.Tabla.equalsIgnoreCase("Departamento")) {
            modeloTabla = (DefaultTableModel) Departamento.DepartamentoT.getModel();

        } else if (Productos.Tabla.equalsIgnoreCase("Compras")) {
            modeloTabla = (DefaultTableModel) Compras.TablaCompras.getModel();
        }

        modeloTabla.setRowCount(0);

        Connection con = Conexion.getConnection();
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int Columnas;
        try {

            ps = con.prepareStatement("Select * From dbo." + Productos.Tabla + "  ");

            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            Columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[Columnas];
                for (int i = 0; i < Columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProyectoBD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void back() {
        Productos.setVisible(true);
        if(ProyectoBD.EDITAR.isVisible()){
            ProyectoBD.EDITAR.setVisible(false);
        }
    }

    public void Guardar(String dato, String dato1, String dato2, String dato3, String dato4, String dato5, String dato6, String dato7) {
        try {

            int id, tel, num_pla;
            String nom, dir, ciu;
            Connection con = Conexion.getConnection();
            PreparedStatement ps = con.prepareStatement("");

            if (Productos.Tabla.equalsIgnoreCase("Productos")) {
                ps = con.prepareStatement("insert into " + Productos.Tabla + "(  Id_Producto, Codigo, Nombre_Prod, Precio_Compra, Precio_Venta, Existencia, Id_Departamento, Id_Marca) values(?,?,?,?,?,?,?,?)");
                ps.setInt(1, Integer.parseInt(dato));
                ps.setInt(2, Integer.parseInt(dato1));
                ps.setString(3, dato2);
                ps.setInt(4, Integer.parseInt(dato3));
                ps.setInt(5, Integer.parseInt(dato4));
                ps.setInt(6, Integer.parseInt(dato5));
                ps.setInt(7, Integer.parseInt(dato6));
                ps.setInt(8, Integer.parseInt(dato7));
            } else if (Productos.Tabla.equalsIgnoreCase("Venta")) {
                ps = con.prepareStatement("insert into " + Productos.Tabla + "(  Id_Venta, Fecha_v, Id_Usuario, Id_Cliente) values(?,?,?,?)");
                ps.setInt(1, Integer.parseInt(dato));
                ps.setString(2, dato1);
                ps.setInt(3, Integer.parseInt(dato2));
                ps.setInt(4, Integer.parseInt(dato3));

            } else if (Productos.Tabla.equalsIgnoreCase("Compras")) {
                ps = con.prepareStatement("insert into " + Productos.Tabla + "(  Id_Compras, Total_Pagar, Id_Proveedor, OtrosGastos,Id_Usuario) values(?,?,?,?,?)");
                ps.setInt(1, Integer.parseInt(dato));
                ps.setString(2, dato1);
                ps.setInt(3, Integer.parseInt(dato2));
                ps.setInt(4, Integer.parseInt(dato3));
                ps.setInt(5, Integer.parseInt(dato4));
            } else if (Productos.Tabla.equalsIgnoreCase("Departamento")) {
                ps = con.prepareStatement("insert into " + Productos.Tabla + "(  Id_Departamento, Nombre_Dep) values(?,?)");
                ps.setInt(1, Integer.parseInt(dato));
                ps.setString(2, dato1);
            }

            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, Productos.Tabla + " Guardado con exito");
            llenarTabla(Productos.Tabla);
        } catch (SQLException ex) {
            Logger.getLogger(Eventos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "error: " + ex.toString());
        }
    }

    public void Eliminar(String dato) {

        try {
            Connection con = Conexion.getConnection();
            PreparedStatement ps = con.prepareStatement("");
            if (Productos.Tabla.equalsIgnoreCase("Productos")) {
                ps = con.prepareStatement("delete " + Productos.Tabla + " where Id_Producto=?");
            } else if (Productos.Tabla.equalsIgnoreCase("Compras")) {
                ps = con.prepareStatement("delete " + Productos.Tabla + " where Id_Compras=?");
            } else if (Productos.Tabla.equalsIgnoreCase("Venta")) {
                ps = con.prepareStatement("delete " + Productos.Tabla + " where Id_Venta=?");
            } else if (Productos.Tabla.equalsIgnoreCase("Departamento")) {
                ps = con.prepareStatement("delete " + Productos.Tabla + " where Id_Departamento=?");
            }
            ps.setInt(1, Integer.parseInt(dato));
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, Productos.Tabla + " borrado con exito");
            llenarTabla(Productos.Tabla);
        } catch (SQLException ex) {
            Logger.getLogger(Eventos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
}
