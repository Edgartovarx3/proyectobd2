CREATE DATABASE ABARROTERA_APACHES
Use ABARROTERA_APACHES
DROP DATABASE ABARROTERA_APACHES

CREATE TABLE Usuarios
(
    Id_Usuario INT NOT NULL PRIMARY KEY,
	NombreUs VARCHAR(50) NOT NULL,
	ApUs VARCHAR(25) NOT NULL,
	AmUs VARCHAR(25) NOT NULL, 
	Direccion VARCHAR(100) NOT NULL,
	Telefono INT NOT NULL,
	Loginn VARCHAR(15) NOT NULL,
	Contraseña VARCHAR(20) NOT NULL,
) 
GO
DROP TABLE Usuarios

CREATE TABLE Marcas
(
	Id_Marca INT NOT NULL PRIMARY KEY,
	Nombre_Marca VARCHAR(25) NOT NULL, 
)
GO
DROP TABLE Marcas

Create Table Departamento
(
	Id_Departamento INT NOT NULL primary key,
	Nombre_Dep VARCHAR(20),
)
Go
DROP TABLE Departamento

Create Table Productos
( 
	Id_Producto INT NOT NULL primary key,
	Codigo VARCHAR(20) NOT NULL,
	Nombre_Prod VARCHAR(75) NOT NULL,
	Precio_Compra DEC NOT NULL,
	Precio_Venta DEC NOT NULL,
	Existencia INT NOT NULL,
	Id_Departamento INT NOT NULL,
	Id_Marca INT NOT NULL,
	CONSTRAINT fk_IdDepartamento FOREIGN KEY (Id_Departamento) REFERENCES Departamento,
	CONSTRAINT fk_IdMarca FOREIGN KEY (Id_Marca) REFERENCES Marcas,
)
GO
DROP TABLE Productos

Create Table Compras
(
	Id_Compras INT NOT NULL PRIMARY KEY,
	Total_Pagar DEC NOT NULL,
	Id_Proveedor INT NOT NULL,
	OtrosGastos DEC NOT NULL,
	Id_Usuario INT NOT NULL,
	CONSTRAINT fk_IdProveedor FOREIGN KEY (Id_Proveedor) REFERENCES Proveedor,
	CONSTRAINT fk_IdUsuario FOREIGN KEY (Id_Usuario) REFERENCES Usuarios,
)
GO
DROP TABLE Compras

Create Table DetalleCompra
(
	Id_Detalle INT NOT NULL PRIMARY KEY,
	Precio DEC NOT NULL,
	Id_Producto INT NOT NULL,
	Id_Compra INT NOT NULL,
	CONSTRAINT fk_IdProducto FOREIGN KEY (Id_Producto) REFERENCES Productos,
	CONSTRAINT fk_IdCompra FOREIGN KEY (Id_Compra) REFERENCES Compras,
)
GO
DROP TABLE DetalleCompra

CREATE TABLE CreditoProveedor
(
    Id_CredP INT NOT NULL PRIMARY KEY,
	IdComprasCredp INT NOT NULL,
	Pago DEC NOT NULL,
	FechaPago DATE NOT NULL,
	Id_Usuario INT NOT NULL,
	CONSTRAINT fk_IdUsuari FOREIGN KEY (Id_Usuario) REFERENCES Usuarios,
) 
GO

CREATE TABLE Proveedor
(
    Id_Proveedor INT NOT NULL PRIMARY KEY,
	NombrePv VARCHAR(50) NOT NULL,
	DireccionPv VARCHAR(100) NOT NULL,
	TelefonoPv INT NOT NULL,
) 
GO
DROP TABLE Proveedor

CREATE TABLE Venta
(
	Id_Venta INT NOT NULL PRIMARY KEY,
	Fecha_v DATE NOT NULL,
	Id_Usuario INT NOT NULL,
	Id_Cliente INT NOT NULL,
	CONSTRAINT fk_IdUsuar FOREIGN KEY (Id_Usuario) REFERENCES Usuarios,
	CONSTRAINT fk_IdCliente FOREIGN KEY (Id_Cliente) REFERENCES Cliente,
)
GO

CREATE TABLE Detalle_Venta
(
	Id_detalle INT NOT NULL PRIMARY KEY,
	Precio_APublico DEC NOT NULL,
	Id_Producto INT NOT NULL,
	Id_Venta INT NOT NULL,
	CONSTRAINT fk_IdProduct FOREIGN KEY (Id_Producto) REFERENCES Productos,
	CONSTRAINT fk_IdVenta FOREIGN KEY (Id_Venta) REFERENCES Venta,
)
GO
DROP TABLE Detalle_Venta

CREATE TABLE Credito_Cliente
(
	Id_CreditoC INT NOT NULL PRIMARY KEY,
	PagoTotalCC DEC NOT NULL,
	FechaPago DATE NOT NULL,
	Id_Usuario INT NOT NULL,
	Id_Venta INT NOT NULL,
	CONSTRAINT fk_IdVentacc FOREIGN KEY (Id_Venta) REFERENCES Venta,
	CONSTRAINT fk_IdUsuariocc FOREIGN KEY (Id_Usuario) REFERENCES Usuarios,
)
GO

CREATE TABLE Cliente
(
	Id_Cliente INT NOT NULL PRIMARY KEY,
	Nombre_Cliente VARCHAR(50) NOT NULL,
	RFC VARCHAR(50) NOT NULL,
	Domicilio VARCHAR(50) NOT NULL,
	Telefono INT NOT NULL,
)
GO
DROP TABLE Cliente
--Insert de tabla Usuarios--
select * from usuarios
INSERT INTO "Usuarios" VALUES(1,'Francisco','Garcia','Mendez','Fresno #3',351456,'12_Fran','1245')
INSERT INTO "Usuarios" VALUES(2,'Efrain','Vazquez','Garcia','Cedro #2',351454,'12_Efra','1246')
INSERT INTO "Usuarios" VALUES(3,'Gustavo','Mendez','Mendez','Vergel #1',351444,'12_Gus','1248')
INSERT INTO "Usuarios" VALUES(4,'Maria','Hernandez','Vazquez','Estancia #5',351432,'12_Mari','1247')
INSERT INTO "Usuarios" VALUES(5,'Josefina','Velazquez','Hernandez','5 de mayo #3',351324,'12_Jos','1242')
INSERT INTO "Usuarios" VALUES(6,'Elena','Martinez','velazquez','Jacona #3',351409,'12_Ele','1285')
INSERT INTO "Usuarios" VALUES(7,'Alejandro','Reyes','Martinez','virrell #3',351254,'12_Alex','1224')
INSERT INTO "Usuarios" VALUES(8,'Carlos','Contreras','Navarro','Atacheco #2',351864,'12_Carl','1248')
INSERT INTO "Usuarios" VALUES(9,'Pedro','Navarro','Reyes','Fresno #6',351458,'12_Ped','1284')
INSERT INTO "Usuarios" VALUES(10,'Jose','Garnica','Navarro','Jamay #7',351447,'12_Jose','1298')
--Insert de tabla Marcas--
select * from marcas
INSERT INTO "Marcas" VALUES(21,'Pandesa')
INSERT INTO "Marcas" VALUES(22,'Bimbo')
INSERT INTO "Marcas" VALUES(23,'Colgate')
INSERT INTO "Marcas" VALUES(24,'Marinela')
INSERT INTO "Marcas" VALUES(25,'Gamesa')
INSERT INTO "Marcas" VALUES(26,'P&G')
INSERT INTO "Marcas" VALUES(27,'La costeña')
INSERT INTO "Marcas" VALUES(28,'La moderna')
INSERT INTO "Marcas" VALUES(29,'Coca Cola')
INSERT INTO "Marcas" VALUES(30,'Sabritas')
--Insert de tabla Departamento--
select * from departamento
INSERT INTO "Departamento" VALUES(101,'Niños')
INSERT INTO "Departamento" VALUES(102,'Bebes')
INSERT INTO "Departamento" VALUES(103,'Niñas')
INSERT INTO "Departamento" VALUES(104,'Limpieza')
INSERT INTO "Departamento" VALUES(105,'Hogar')
INSERT INTO "Departamento" VALUES(106,'Higiene Personal')
INSERT INTO "Departamento" VALUES(107,'Deportes')
INSERT INTO "Departamento" VALUES(108,'Ejercicio')
INSERT INTO "Departamento" VALUES(109,'Farmacia')
INSERT INTO "Departamento" VALUES(110,'Vehiculos')
--Insert de tabla Productos--
select * from productos
INSERT INTO "Productos" VALUES(201,'1379','Playera',60.00,79.99,12,101,26)
INSERT INTO "Productos" VALUES(202,'1479','Llanta',500.00,699.99,24,110,23)
INSERT INTO "Productos" VALUES(203,'1579','Fideos',2.00,4.99,5,105,28)
INSERT INTO "Productos" VALUES(204,'1679','Vestido',60.00,79.99,6,103,26)
INSERT INTO "Productos" VALUES(205,'1779','Petobismol',40.00,69.99,40,109,26)
INSERT INTO "Productos" VALUES(206,'1879','Cloro',50.00,79.99,54,104,25)
INSERT INTO "Productos" VALUES(207,'1979','Shampoo',40.00,69.99,1,106,23)
INSERT INTO "Productos" VALUES(208,'2079','Powerade',15.00,24.99,7,105,29)
INSERT INTO "Productos" VALUES(209,'2179','Chiles jalapeños',12.00,19.99,58,105,27)
INSERT INTO "Productos" VALUES(210,'2279','Pan tostado',16.00,27.99,23,105,22)
--Insert de tabla Compras--
select * from compras
INSERT INTO Compras Values (00001, 499.50, 1001, 50.21,1);
INSERT INTO Compras Values (00002, 213.50, 1002, 70.12,2);
INSERT INTO Compras Values (00003, 577.50, 1003, 20.41,3);
INSERT INTO Compras Values (00004, 324.50, 1004, 10.14,4);
INSERT INTO Compras Values (00005, 678.50, 1005, 5.15,5);
INSERT INTO Compras Values (00006, 236.50, 1006, 35.54,6);
INSERT INTO Compras Values (00007, 623.50, 1007, 15.99,7);
INSERT INTO Compras Values (00008, 923.50, 1008, 42.06,8);
INSERT INTO Compras Values (00009, 456.50, 1009, 64.03,9);
INSERT INTO Compras Values (00010, 876.50, 1010, 21.49,10);
--Insert de tabla DetalleCompra--
select * from detallecompra
INSERT INTO DetalleCompra Values (901, 49.99, 201, 00001);
INSERT INTO DetalleCompra Values (902, 76.99, 202, 00002);
INSERT INTO DetalleCompra Values (903, 98.99, 203, 00003);
INSERT INTO DetalleCompra Values (904, 34.99, 204, 00004);
INSERT INTO DetalleCompra Values (905, 61.99, 205, 00005);
INSERT INTO DetalleCompra Values (906, 63.89, 206, 00006);
INSERT INTO DetalleCompra Values (907, 71.53, 207, 00007);
INSERT INTO DetalleCompra Values (908, 29.34, 208, 00008);
INSERT INTO DetalleCompra Values (909, 58.67, 209, 00009);
INSERT INTO DetalleCompra Values (910, 42.45, 210, 00010);
--Insert de tabla CreditoProvedor--
select * from creditoproveedor
select * from proveedor
select * from usuarios
INSERT INTO CreditoProveedor Values (2001, 3001, 456.79, '2022-04-01', 1);
INSERT INTO CreditoProveedor Values (2002, 3002, 345.69, '2022-04-02', 2);
INSERT INTO CreditoProveedor Values (2003, 3003, 234.59, '2022-04-03', 3);
INSERT INTO CreditoProveedor Values (2004, 3004, 723.49, '2022-04-04', 4);
INSERT INTO CreditoProveedor Values (2005, 3005, 823.39, '2022-04-05', 5);
INSERT INTO CreditoProveedor Values (2006, 3006, 842.29, '2022-04-06', 6);
INSERT INTO CreditoProveedor Values (2007, 3007, 479.19, '2022-04-07', 7);
INSERT INTO CreditoProveedor Values (2008, 3008, 549.98, '2022-04-07', 8);
INSERT INTO CreditoProveedor Values (2009, 3009, 498.94, '2022-04-07', 9);
INSERT INTO CreditoProveedor Values (2010, 3010, 249.93, '2022-04-07', 10);
--Insert de tabla Proveedor--

INSERT INTO Proveedor Values (1001, 'Francisco', 'Benito Juarez 1022', 89032365);
INSERT INTO Proveedor Values (1002, 'Jose', 'Madero 325', 89032233);
INSERT INTO Proveedor Values (1003, 'Daniel', 'Reforma 54', 89032456);
INSERT INTO Proveedor Values (1004, 'Ramon', 'Mar 22', 89032635);
INSERT INTO Proveedor Values (1005, 'Manuel', 'Lluvia 78', 89032180);
INSERT INTO Proveedor Values (1006, 'Andres', 'Zapopan 98', 89032263);
INSERT INTO Proveedor Values (1007, 'Juan', 'Morelia 53', 89032791);
INSERT INTO Proveedor Values (1008, 'Hector', 'Uruapan 35', 89032083);
INSERT INTO Proveedor Values (1009, 'Ulises', 'Jalisco 657', 89032832);
INSERT INTO Proveedor Values (1010, 'Alejandro', 'Orandino 523', 89032865);
--Insert de tabla Venta--
select * from venta
INSERT INTO "Venta" VALUES(501,'2022-05-08',1,801)
INSERT INTO "Venta" VALUES(502,'2022-05-10',2,802)
INSERT INTO "Venta" VALUES(503,'2022-05-14',3,803)
INSERT INTO "Venta" VALUES(504,'2022-05-23',4,804)
INSERT INTO "Venta" VALUES(505,'2022-05-22',5,805)
INSERT INTO "Venta" VALUES(506,'2022-05-05',6,806)
INSERT INTO "Venta" VALUES(507,'2022-05-09',7,807)
INSERT INTO "Venta" VALUES(508,'2022-05-30',8,808)
INSERT INTO "Venta" VALUES(509,'2022-05-16',9,809)
INSERT INTO "Venta" VALUES(510,'2022-05-04',10,810)
--Insert de tabla Detalle_Venta--
select * from detalle_venta
INSERT INTO "Detalle_Venta" VALUES(601,12.00,201,501)
INSERT INTO "Detalle_Venta" VALUES(602,12.00,202,502)
INSERT INTO "Detalle_Venta" VALUES(603,12.00,203,503)
INSERT INTO "Detalle_Venta" VALUES(604,12.00,204,504)
INSERT INTO "Detalle_Venta" VALUES(605,12.00,205,505)
INSERT INTO "Detalle_Venta" VALUES(606,12.00,206,506)
INSERT INTO "Detalle_Venta" VALUES(607,12.00,207,507)
INSERT INTO "Detalle_Venta" VALUES(608,12.00,208,508)
INSERT INTO "Detalle_Venta" VALUES(609,12.00,209,509)
INSERT INTO "Detalle_Venta" VALUES(610,12.00,210,510)
--Insert de tabla Credito_Cliente--
select * from credito_cliente
INSERT INTO "Credito_Cliente" VALUES(701,120.00,'2022-05-06',1,501)
INSERT INTO "Credito_Cliente" VALUES(702,210.00,'2022-05-07',2,502)
INSERT INTO "Credito_Cliente" VALUES(703,301.00,'2022-05-10',3,503)
INSERT INTO "Credito_Cliente" VALUES(704,405.00,'2022-05-25',4,504)
INSERT INTO "Credito_Cliente" VALUES(705,48.00,'2022-05-15',5,505)
INSERT INTO "Credito_Cliente" VALUES(706,600.00,'2022-05-30',6,506)
INSERT INTO "Credito_Cliente" VALUES(707,200.00,'2022-05-02',7,507)
INSERT INTO "Credito_Cliente" VALUES(708,1000.00,'2022-05-04',8,508)
INSERT INTO "Credito_Cliente" VALUES(709,145.00,'2022-05-07',9,509)
INSERT INTO "Credito_Cliente" VALUES(710,126.00,'2022-05-29',10,510)
--Insert de tabla Cliente--
select * from cliente
INSERT INTO "Cliente" VALUES(801,'Luis','MEPL010501416','Jacona',351)
INSERT INTO "Cliente" VALUES(802,'Fernando','MEPL010501417','Zamora',126)
INSERT INTO "Cliente" VALUES(803,'Ricardo','MEPL010501418','Atacheo',456)
INSERT INTO "Cliente" VALUES(804,'Edgar','MEPL010501419','Vergel',245)
INSERT INTO "Cliente" VALUES(805,'Arturo','MEPL010501420','Janitzio',879)
INSERT INTO "Cliente" VALUES(806,'Maria','MEPL010501421','Palo alto',254)
INSERT INTO "Cliente" VALUES(807,'Jesus','MEPL010501422','Virrell',479)
INSERT INTO "Cliente" VALUES(808,'Rafael','MEPL010501423','Jamay',213)
INSERT INTO "Cliente" VALUES(809,'Karla','MEPL010501424','Guanajuato',546)
INSERT INTO "Cliente" VALUES(810,'Javier','MEPL010501425','Estancia',147)

create database clientes----- fragmentación para corrobar a nuestros clientes cuya r aparezca en cualquier parte de su nombre
select * into clientes.dbo.Cliente from ABARROTERA_APACHES.dbo.Cliente C where C.Nombre_Cliente like 'r%'
select * from clientes.dbo.Cliente

create database pagocreditoproveedor -------fragmentación para corroborar los pagos hechos despues de la fecha del 6 de abril del año en curso
select * into   pagocreditoproveedor.dbo.creditoproveedor from ABARROTERA_APACHES.dbo.CreditoProveedor CP where CP.FechaPago>='2022-04-06'
select * from   pagocreditoproveedor.dbo.creditoproveedor

create database ventadetails--- fragmentación para dividir y corroborar las ventas cuyo id sea mayor al 505
select * into ventadetails.dbo.Venta from ABARROTERA_APACHES.dbo.Venta V where V.Id_Venta>=505
select * from ventadetails.dbo.Venta
create database usuarios1a5---fragmentación para dividir a los usuarios 
select * into usuarios1a5.dbo.usuarios from ABARROTERA_APACHES.dbo.Usuarios U where U.Id_Usuario<=5
select * from usuarios1a5.dbo.usuarios
create database usuarios6a10
select * into usuarios6a10.dbo.usuarios from ABARROTERA_APACHES.dbo.Usuarios U where U.Id_Usuario>=6
 select * from usuarios1a5.dbo.usuarios
 union
select * from usuarios6a10.dbo.usuarios
create database departamentosh ----mostrar los departamentos con letra incial h
select * into departamentosh.dbo.departamento from ABARROTERA_APACHES.dbo.Departamento D where D.Nombre_Dep like '%h'

create database productos_baratos---division de ls productos de un menor costo y de mayor costo 
select * into productos_baratos.dbo.productos from ABARROTERA_APACHES.dbo.Productos P where P.Precio_Compra<=40
select * from productos_baratos.dbo.productos
create database productos_caros
select * into productos_caros.dbo.productos from ABARROTERA_APACHES.dbo.Productos P where P.Precio_Compra>=50
select * from productos_caros.dbo.productos

create database comprasdemayorimporte--------fragmentación para dividir las compras que tienen un imprte mayor a los 500 pesos
select * into comprasdemayorimporte.dbo.compras from ABARROTERA_APACHES.dbo.Compras C where C.Total_Pagar>500
select * from comprasdemayorimporte.dbo.compras

create database galletas2---- frgamentación para solo observar que marcas de galletas se ofrecen
select * into galletas2.dbo.marcas from ABARROTERA_APACHES.dbo.Marcas M where M.Nombre_Marca like 'G%'
select * from  galletas2.dbo.m